package com.maxim.faiflytestapplication.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.maxim.faiflytestapplication.R;
import com.maxim.faiflytestapplication.model.CityInformation;

import java.util.List;

/**
 * Created by Maxim Shemshey on 07.10.2017.
 */

public class CityDetailAdapter extends RecyclerView.Adapter<CityDetailAdapter.ViewHolder>{

    private Context context;
    private List<CityInformation> detailList;


    public CityDetailAdapter(Context context, List<CityInformation> detailList) {
        this.context = context;
        this.detailList = detailList;
    }


    @Override
    public CityDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(context).inflate(R.layout.item_detail_view, parent, false);
        return new ViewHolder(item);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CityInformation cityInformation = detailList.get(position);
        holder.tvSummary.setText(cityInformation.getSummary());
        holder.tvTitle.setText(cityInformation.getTitle());
        holder.tvWikipediaUrl.setText(cityInformation.getWikipediaUrl());
    }

    @Override
    public int getItemCount() {
        return detailList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvSummary;
        private TextView tvTitle;
        private TextView tvWikipediaUrl;

        ViewHolder(View item) {
            super(item);
            tvSummary = item.findViewById(R.id.textView_summary);
            tvTitle = item.findViewById(R.id.textView_title);
            tvWikipediaUrl = item.findViewById(R.id.textView_wikipediaUrl);

        }
    }
}
