package com.maxim.faiflytestapplication.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.maxim.faiflytestapplication.R;

import java.util.List;

/**
 * Created by Maxim Shemshey on 06.10.2017.
 */

public class CitiesByCountryAdapter extends RecyclerView.Adapter<CitiesByCountryAdapter.ViewHolder> {

    public interface CityClickListener {
        void onClick(String city);
    }

    interface ItemClickListener {
        void onClick(int position);
    }

    private Context context;
    private List<String> citiesList;
    private CitiesByCountryAdapter.CityClickListener cityClickListener;
    private CitiesByCountryAdapter.ItemClickListener itemClickListener = new ItemClickListener() {
        @Override
        public void onClick(int position) {
            cityClickListener.onClick(citiesList.get(position));
        }
    };

    public CitiesByCountryAdapter(Context context, List<String> citiesList, CityClickListener cityClickListener) {
        this.context = context;
        this.citiesList = citiesList;
        this.cityClickListener = cityClickListener;
    }

    @Override
    public CitiesByCountryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(context).inflate(R.layout.item_city_view, parent, false);
        return new ViewHolder(item);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String city = citiesList.get(position);
        holder.tvCity.setText(city);
    }

    @Override
    public int getItemCount() {
        return citiesList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvCity;

        ViewHolder(View item) {
            super(item);
            tvCity = item.findViewById(R.id.textView_city);
            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onClick(getAdapterPosition());
                }
            });
        }
    }
}
