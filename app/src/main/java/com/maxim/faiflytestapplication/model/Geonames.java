package com.maxim.faiflytestapplication.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Maxim Shemshey on 06.10.2017.
 */

public class Geonames implements Serializable {

    private List<CityInformation> geonames;

    public Geonames(List<CityInformation> geonames) {
        this.geonames = geonames;
    }

    public List<CityInformation> getGeonames() {
        return geonames;
    }
}
