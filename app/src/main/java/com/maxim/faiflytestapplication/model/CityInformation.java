package com.maxim.faiflytestapplication.model;

import java.io.Serializable;

/**
 * Created by Maxim Shemshey on 06.10.2017.
 */

public class CityInformation implements Serializable {

    private String summary;
    private String title;
    private String wikipediaUrl;

    public CityInformation(String summary, String title, String wikipediaUrl) {
        this.summary = summary;
        this.title = title;
        this.wikipediaUrl = wikipediaUrl;
    }

    public String getSummary() {
        return summary;
    }

    public String getTitle() {
        return title;
    }

    public String getWikipediaUrl() {
        return wikipediaUrl;
    }

}
