package com.maxim.faiflytestapplication.ui;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.maxim.faiflytestapplication.DataBaseHelper;
import com.maxim.faiflytestapplication.R;
import com.maxim.faiflytestapplication.adapter.CitiesByCountryAdapter;
import com.maxim.faiflytestapplication.retrofit.RetrofitCountriesToCities;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.maxim.faiflytestapplication.DataBaseHelper.KEY_CITY_NAME;
import static com.maxim.faiflytestapplication.DataBaseHelper.KEY_COUNTRY_ID;
import static com.maxim.faiflytestapplication.DataBaseHelper.KEY_COUNTRY_NAME;
import static com.maxim.faiflytestapplication.DataBaseHelper.KEY_COUNTRY_SECONDARY_ID;
import static com.maxim.faiflytestapplication.DataBaseHelper.TABLE_CITIES;
import static com.maxim.faiflytestapplication.DataBaseHelper.TABLE_COUNTRIES;

public class MainActivity extends AppCompatActivity {

    public static final String KEY = "city";
    private static boolean isApplicationJustOpened = true;

    private DataBaseHelper dataBaseHelper;
    private ArrayAdapter<String> adapterCountries;
    private Spinner spinner;

    private RecyclerView recyclerView;

    Map<String, List<String>> citiesByCountryMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        dataBaseHelper = new DataBaseHelper(this);

        spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setVisibility(View.INVISIBLE);
        adapterCountries = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, new ArrayList<String>(0));
        spinner.setPrompt("Country");
        adapterCountries.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_city);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String countryName = (String) parent.getItemAtPosition(position);
                final List<String> cities = citiesByCountryMap.get(countryName);
                Collections.sort(cities);
                CitiesByCountryAdapter citiesByCountryAdapter = new CitiesByCountryAdapter(MainActivity.this, cities, new CitiesByCountryAdapter.CityClickListener() {
                    @Override
                    public void onClick(final String city) {
                        Intent intent = new Intent(getBaseContext(), CityDetailActivity.class);
                        intent.putExtra(KEY, city);
                        startActivity(intent);
                    }
                });
                recyclerView.setAdapter(citiesByCountryAdapter);
                citiesByCountryAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner.setAdapter(adapterCountries);


        if (isApplicationJustOpened) {
            isApplicationJustOpened = false;
            loadDataFromInternet();
        }

        citiesByCountryMap = initDataFromDataBase();
        updateSpinner();
    }

    @Override
    protected void onDestroy() {
        dataBaseHelper.close();
        super.onDestroy();
    }

    private void updateSpinner() {
        final List<String> countries = new ArrayList(citiesByCountryMap.keySet());
        Collections.sort(countries);

        if (!countries.isEmpty()) {
            spinner.setVisibility(View.VISIBLE);
            adapterCountries.clear();
            adapterCountries.addAll(countries);
            adapterCountries.notifyDataSetChanged();
        }
    }

    private Map<String, List<String>> initDataFromDataBase() {
        Map<String, List<String>> citiesByCountryMap = new HashMap<>();

        Map<Integer, String> countriesMap = new HashMap<>();

        // Put in Map all countries as key with empty city list
        Cursor countriesCursor = dataBaseHelper.getCountries();
        while (countriesCursor.moveToNext()) {
            String countryName = countriesCursor.getString(countriesCursor.getColumnIndex(KEY_COUNTRY_NAME));
            int countryId = countriesCursor.getInt(countriesCursor.getColumnIndex(KEY_COUNTRY_ID));
            countriesMap.put(countryId, countryName);
            citiesByCountryMap.put(countryName, new ArrayList<String>());
        }

        Cursor citiesCursor = dataBaseHelper.getCities();
        while (citiesCursor.moveToNext()) {
            String cityName = citiesCursor.getString(citiesCursor.getColumnIndex(KEY_CITY_NAME));
            int secondaryId = citiesCursor.getInt(citiesCursor.getColumnIndex(KEY_COUNTRY_SECONDARY_ID));

            String countryName = countriesMap.get(secondaryId);
            citiesByCountryMap.get(countryName).add(cityName);
        }
        return citiesByCountryMap;
    }

    private void updateDataBaseIfNeeded(Map<String, List<String>> citiesByCountryMap) {
        final List<String> countries = new ArrayList<>(citiesByCountryMap.keySet());
        Collections.sort(countries);

        // Insert countries in database if needed
        final Cursor countriesRowNumberCursor = dataBaseHelper.getRowNumberCursor(TABLE_COUNTRIES);
        if (countriesRowNumberCursor.moveToFirst()) {
            int countCountry = countriesRowNumberCursor.getInt(0);
            if (countries.size() == countCountry) {
                // It means that we have all data from json and don't need to update database
            } else {
                // We should delete existing table and create new, insert new information
                dataBaseHelper.deleteTable(TABLE_COUNTRIES);
                dataBaseHelper.createCountriesTable();
                dataBaseHelper.insertAllCountries(countries);
            }
        }

        // Count number of cities from json
        int citiesNumber = 0;
        for (final String country : countries) {
            citiesNumber += citiesByCountryMap.get(country).size();
        }

        // Insert cities in database if needed
        final Cursor citiesRowNumberCursor = dataBaseHelper.getRowNumberCursor(TABLE_CITIES);
        if (citiesRowNumberCursor.moveToFirst()) {
            int countCity = citiesRowNumberCursor.getInt(0);

            // My bad, I have no idea why but countCity is less by one so +1
            if (citiesNumber == countCity + 1) {
                // It means that we have all data from json and don't need to update database
            } else {
                // We should delete existing table and create new, insert new information
                dataBaseHelper.deleteTable(TABLE_CITIES);
                dataBaseHelper.createCitiesTable();
                // get all countries with ids
                final long millisStart = Calendar.getInstance().getTimeInMillis();
                final Cursor allCountriesCursor = dataBaseHelper.getCountries();
                final long millisEnd = Calendar.getInstance().getTimeInMillis();

                Map<String, Integer> countriesMap = new HashMap<>();
                while (allCountriesCursor.moveToNext()) {
                    int id = allCountriesCursor.getInt(allCountriesCursor.getColumnIndex(KEY_COUNTRY_ID));
                    String name = allCountriesCursor.getString(allCountriesCursor.getColumnIndex(KEY_COUNTRY_NAME));
                    countriesMap.put(name, id);
                }
                for (final String country : countries) {
                    if (!country.equals("")) {
                        final List<String> cities = citiesByCountryMap.get(country);
                        Collections.sort(cities);
                        dataBaseHelper.insertCities(countriesMap.get(country), cities);
                    }
                }
            }
        }
    }

    private void loadDataFromInternet() {
        RetrofitCountriesToCities.getInformation(new Callback<Map<String, List<String>>>() {

            @Override
            public void success(Map<String, List<String>> map, Response response) {
                // FIXME move this update to IntentSevice
                updateDataBaseIfNeeded(map);
                citiesByCountryMap = map;
                updateSpinner();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), "Exception with HTTP!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

