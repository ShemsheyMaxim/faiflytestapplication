package com.maxim.faiflytestapplication.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.maxim.faiflytestapplication.R;
import com.maxim.faiflytestapplication.adapter.CityDetailAdapter;
import com.maxim.faiflytestapplication.model.CityInformation;
import com.maxim.faiflytestapplication.model.Geonames;
import com.maxim.faiflytestapplication.retrofit.RetrofitCityInformation;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CityDetailActivity extends AppCompatActivity {

    private List<CityInformation> detailList = new ArrayList<>(0);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.city_detail);

        final String nameCity = getIntent().getStringExtra(MainActivity.KEY);
        final CityDetailAdapter cityDetailAdapter = new CityDetailAdapter(getBaseContext(), detailList);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list_view_detail);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(cityDetailAdapter);

        RetrofitCityInformation.getCity(nameCity, new Callback<Geonames>() {
            @Override
            public void success(Geonames geonames, Response response) {

                detailList.addAll(geonames.getGeonames());
                cityDetailAdapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), "Exception with HTTP!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}