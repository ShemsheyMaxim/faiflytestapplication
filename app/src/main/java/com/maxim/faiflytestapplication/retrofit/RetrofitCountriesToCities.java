package com.maxim.faiflytestapplication.retrofit;

import java.util.List;
import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;

/**
 * Created by Maxim Shemshey on 05.10.2017.
 */

public class RetrofitCountriesToCities {

    private static final String ENDPOINT = "https://raw.githubusercontent.com";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface {
        @GET("/David-Haim/CountriesToCitiesJSON/master/countriesToCities.json")
        void getInformation(Callback<Map<String, List<String>>> callback);
    }

    private static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getInformation(Callback<Map<String, List<String>>> callback) {
        apiInterface.getInformation(callback);
    }
}
