package com.maxim.faiflytestapplication.retrofit;

import com.maxim.faiflytestapplication.model.Geonames;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Maxim Shemshey on 06.10.2017.
 */

public class RetrofitCityInformation {

    private static final String ENDPOINT = "http://api.geonames.org";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface {
        @GET("/findNearbyWikipediaJSON")
        void getCity(@Query("placename") String city, @Query("username") String name, Callback<Geonames> callback);
    }

    private static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getCity(String city, Callback<Geonames> callback) {
        apiInterface.getCity(city, "shanhaymax", callback);
    }
}
