package com.maxim.faiflytestapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.List;

/**
 * Created by Maxim Shemshey on 05.10.2017.
 */

public class DataBaseHelper extends SQLiteOpenHelper {

    private static String DB_NAME = "DataBase";
    private static int DB_VERSION = 16;

    public static String TABLE_COUNTRIES = "Countries";
    public static final String KEY_COUNTRY_ID = "country_id";
    public static final String KEY_COUNTRY_NAME = "country_name";

    public static String TABLE_CITIES = "Cities";
    public static final String KEY_CITY_ID = "city_id";
    public static final String KEY_CITY_NAME = "city_name";
    public static final String KEY_COUNTRY_SECONDARY_ID = "country_secondary_id";


    private static final String DELETE_TABLE_QUERY = "DROP TABLE IF EXISTS %1$s";
    private static final String COUNT_ROW_NUMBER_QUERY = "SELECT count(*) FROM %1$s";
    private static final String SELECT_ALL_QUERY = "SELECT * FROM %1$s";

    public DataBaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createCountriesTable(db);
        createCitiesTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        deleteTable(db, TABLE_COUNTRIES);
        deleteTable(db, TABLE_CITIES);
        onCreate(db);
    }

    public void createCountriesTable() {
        createCountriesTable(this.getWritableDatabase());
    }

    private void createCountriesTable(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_COUNTRIES + "(" + KEY_COUNTRY_ID
                + " integer primary key autoincrement," + KEY_COUNTRY_NAME + " text" + ")");
    }

    public void createCitiesTable() {
        createCitiesTable(this.getWritableDatabase());
    }

    private void createCitiesTable(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_CITIES + "(" + KEY_CITY_ID
                + " integer primary key autoincrement," + KEY_CITY_NAME + " text, "
                + KEY_COUNTRY_SECONDARY_ID + " integer" + ")");
    }

    public void deleteTable(String tableName) {
        deleteTable(this.getWritableDatabase(), tableName);
    }

    private void deleteTable(SQLiteDatabase db, String tableName) {
        db.execSQL(String.format(DELETE_TABLE_QUERY, tableName));
    }

    public Cursor getRowNumberCursor(String tableName) {
        SQLiteDatabase db = this.getReadableDatabase();
        String countRows = String.format(COUNT_ROW_NUMBER_QUERY, tableName);
        return db.rawQuery(countRows, null);
    }

    public Cursor getCountries() {
        SQLiteDatabase db = this.getReadableDatabase();
        String countRows = String.format(SELECT_ALL_QUERY, TABLE_COUNTRIES);
        return db.rawQuery(countRows, null);
    }

    public Cursor getCities() {
        SQLiteDatabase db = this.getReadableDatabase();
        String countRows = String.format(SELECT_ALL_QUERY, TABLE_CITIES);
        return db.rawQuery(countRows, null);
    }

    public void insertAllCountries(final List<String> countries) {
        SQLiteDatabase db = this.getWritableDatabase();

        for (final String country : countries) {
            ContentValues cvCountry = new ContentValues();
            cvCountry.put(KEY_COUNTRY_NAME, country);
            db.insert(TABLE_COUNTRIES, null, cvCountry);
        }
    }

    public void insertCities(final int countryId, final List<String> cities) {
        SQLiteDatabase db = this.getWritableDatabase();

        for (final String city : cities) {
            ContentValues cvCity = new ContentValues();
            cvCity.put(KEY_CITY_NAME, city);
            cvCity.put(KEY_COUNTRY_SECONDARY_ID, countryId);
            db.insert(TABLE_CITIES, null, cvCity);
        }
    }
}
